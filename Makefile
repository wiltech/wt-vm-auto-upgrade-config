NAME=wt-vm-auto-upgrade-config
VERSION=1.0

PKG_NAME=$(NAME)
PKG_ARCH=all

SYSCONF=/etc
PREFIX?=/usr/share
DOC_DIR=$(PREFIX)/doc/$(PKG_NAME)

BLD_DIR=$(PKG_NAME)_$(VERSION)_$(PKG_ARCH)
PKG=$(BLD_DIR).deb

PUBKEY_ID=EE8BEF82958D1FF33F3B375EBBBB9FB338982697

pkg: build
	dpkg --build ${BLD_DIR}

build:
	mkdir -p $(BLD_DIR)/$(DOC_DIR)
	mkdir -p $(BLD_DIR)/$(SYSCONF)/apt/apt.conf.d
	cp -r config/* $(BLD_DIR)/$(SYSCONF)/apt/apt.conf.d
	cp -r DEBIAN $(BLD_DIR)
	cp LICENSE $(BLD_DIR)/$(DOC_DIR)

sign:
	dpkg-sig -k $(PUBKEY_ID) --sign wiltech $(PKG)
	dpkg-sig -c $(PKG)

clean:
	rm -rf $(PKG) $(BLD_DIR)

all: pkg sign

tag:
	git tag $(VERSION)
	git push --tags

release: pkg sign tag

install:
	apt install ./$(PKG)

uninstall:
	apt remove --purge $(PKG_NAME)

.PHONY: build sign clean test tag release install uninstall all
